class Geonet < Formula
  desc "Geographic Network Tools"
  homepage "https://gitlab.com/shodan-public/geonet-rs"
  url "https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/latest/geonet_latest_darwin_amd64.tar.gz"
  version "0.4.3"
  sha256 "94d344a0acb3e836e5a016e4ddfcc389c5bbc85a46a454c5f4c84c50090acb5d"
  license "MIT"

  on_macos do
    on_arm do
      url "https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/latest/geonet_latest_darwin_arm64.tar.gz"
      sha256 "a7beb90f6877ce6d43f9d79c0332caad4f18334a8852f6227d2a56722a0fc398"
    end
  end

  def install
    bin.install "geodns-#{version}-darwin-#{Hardware::CPU.arch}" => "geodns"
    bin.install "geoping-#{version}-darwin-#{Hardware::CPU.arch}" => "geoping"
  end

  test do
    system "#{bin}/geodns", "google.com"
    system "#{bin}/geoping", "8.8.8.8"
  end
end
