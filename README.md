# Homebrew package for [Geographic Network Tools](https://gitlab.com/shodan-public/geonet-rs)

<p>
<a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-_red.svg"></a>
<a href="https://twitter.com/shodanhq"><img src="https://img.shields.io/twitter/follow/shodanhq.svg?logo=twitter"></a>
</p>

The package currently support these Apple architectures: **amd64**, **arm64**.

## Installation

As Gitlab is not a default repository for **brew** so we need to specific full URL when tap.

```shell
$ brew tap shodan-public/homebrew-geonet https://gitlab.com/shodan-public/homebrew-geonet
$ brew install geonet
```

The binaries are located in **homebrew** exported binaries location, so easy run with:

```shell
$ geodns google.com
$ geoping 8.8.8.8
```

## Upgrading

```shell
$ brew update
$ brew upgrade geonet
```

## Deployment

By default, **Formula/geonet.rb** get updated automatically when **homebrew** pipeline from [geonet](https://gitlab.com/shodan-public/geonet-rs) runs, which it patch two things:

- `sha256` checksum for `amd64` and `arm64` packages.
- `version`

## Troubleshooting

If there are any conflicts when upgrade, could be git upstream commits failed to merge. Untap the repo and reinstall it.

```shell
$ brew untap shodan-public/homebrew-geonet
$ brew tap shodan-public/homebrew-geonet https://gitlab.com/shodan-public/homebrew-geonet
$ brew reinstall geonet
```

## TODO

Publish the package to [homebrew-core](https://github.com/Homebrew/homebrew-core) to make it available by default.